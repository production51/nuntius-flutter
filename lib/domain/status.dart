class Status {
  final DateTime sentAt;
  final DateTime? deliveredAt;
  final DateTime? readAt;

  const Status({
    required this.sentAt,
    this.deliveredAt,
    this.readAt,
  });

  factory Status.fromJson(Map<String, dynamic> json) {
    return Status(
      sentAt: DateTime.parse(json['sent_at']),
      deliveredAt: json['delivered_at'] == null ? null : DateTime.parse(json['delivered_at']),
      readAt: json['read_at'] == null ? null : DateTime.parse(json['read_at']),
    );
  }
}

final datetimeZero = DateTime(1);

enum StatusUpdate {
  delivered,
  read,
}

extension StatusUpdateExtension on StatusUpdate {
  String get value {
    switch (this) {
      case StatusUpdate.delivered:
        return 'delivered';
      case StatusUpdate.read:
        return 'read';
    }
  }
}
