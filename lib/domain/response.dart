import 'package:nuntius_flutter/domain/message.dart';
import 'package:nuntius_flutter/domain/status.dart';

class BackendResponse {
  final int status;
  final ResponseData? data;
  final String? error;

  const BackendResponse({
    required this.status,
    this.data,
    this.error,
  });

  factory BackendResponse.fromJson(Map<String, dynamic> json) {
    return BackendResponse(
      status: json['status'],
      data: json['data'] == null ? null : ResponseData.fromJson(json['data']),
      error: json['error'],
    );
  }

  @override
  String toString() {
    return "Status: $status\nData: $data\nError: $error";
  }
}

class ResponseData {
  final String? id;
  final List<Message>? messages;
  final Status? status;

  const ResponseData({
    this.id,
    this.messages,
    this.status,
  });

  ResponseData.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        messages = json['messages'] == null //
            ? null
            : (json['messages'] as Iterable<dynamic>).map((msg) => Message.fromJson(msg as Map<String, dynamic>)).toList(),
        status = json['status'] == null //
            ? null
            : Status.fromJson(json['status']);

  @override
  String toString() {
    return "Id: $id\nMessages: $messages\nStatus: $status";
  }
}
