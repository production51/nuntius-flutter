import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:nuntius_flutter/config.dart';
import 'package:nuntius_flutter/domain/message.dart';
import 'package:nuntius_flutter/domain/response.dart';
import 'package:nuntius_flutter/domain/status.dart';
import 'package:uuid_type/uuid_type.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

class MessageManager extends ChangeNotifier {
  String? user;
  final Map<String, Map<Uuid, Message>> messages = {};
  StreamSubscription? websocket;

  void setUser(String user) {
    this.user = user;
    messages.clear();
    if (websocket == null) {
      listenOnNewMessages(true);
    } else {
      websocket!.cancel().then((_) => listenOnNewMessages(true));
    }
    notifyListeners();
  }

  void listenOnNewMessages(bool fetchOld) {
    WebSocketChannel newMessagesChan = WebSocketChannel.connect(
      Uri.parse('ws://$backendHost/subscribe?user=$user&fetch_old=$fetchOld'),
    );
    websocket = newMessagesChan.stream.listen(
      (event) {
        // event is a string
        Message message = Message.fromJson(json.decode(event));
        add(message);
        if (message.receiverName == user && message.status.deliveredAt == null) {
          updateMessageStatus(message.id, StatusUpdate.delivered);
        }
      },
      onDone: () {
        listenOnNewMessages(false);
      },
      onError: (error) => print("error upon listening on new messages: $error"),
      cancelOnError: true,
    );
  }

  void updateMessageStatus(Uuid messageId, StatusUpdate statusUpdate) {
    http.patch(
      Uri.parse("http://$backendHost/messages"),
      body: <String, String>{
        "message_id": messageId.toString(),
        "status": statusUpdate.value,
      },
    ).then((response) {
      BackendResponse parsedResponse = BackendResponse.fromJson(json.decode(response.body));
      if (parsedResponse.status != 200) {
        print("error upon updating message status: ${parsedResponse.error}");
      }
    });
  }

  void add(Message message) {
    if (message.senderName != user && message.receiverName != user) {
      return;
    }
    final converser = message.senderName == user //
        ? message.receiverName
        : message.senderName;
    if (!messages.containsKey(converser)) {
      // new converser
      messages[converser] = {};
    }
    // replace message with newer version (may be duplicate or new status)
    messages[converser]![message.id] = message;
    notifyListeners();
  }

  Iterable<Converser> getConversers() {
    return messages.entries.map(
      (entry) => Converser(
        name: entry.key,
        unreadMessagesCount: entry.value.values.where((msg) => user == msg.receiverName && msg.status.readAt == null).length,
      ),
    );
  }

  List<DayMessages> getConversation(String converser) {
    if (!messages.containsKey(converser)) {
      return [];
    }
    final l = messages[converser]!.values.toList();
    l.sort((m1, m2) => m1.status.sentAt.isAfter(m1.status.sentAt) ? 1 : -1);

    List<DayMessages> conversation = [];
    DayMessages? current;
    for (final msg in l) {
      final msgSentDate = DateTime(msg.status.sentAt.year, msg.status.sentAt.month, msg.status.sentAt.day);
      if (current == null || current.date != msgSentDate) {
        if (current != null) conversation.add(current);
        current = DayMessages(date: msgSentDate, messages: []);
      }
      current.messages.add(msg);
    }
    conversation.add(current!);
    return conversation;
  }
}

class DayMessages {
  final DateTime date;
  final List<Message> messages;

  DayMessages({
    required this.date,
    required this.messages,
  });
}

class Converser {
  final String name;
  int unreadMessagesCount = 0;

  Converser({
    required this.name,
    this.unreadMessagesCount = 0,
  });
}
