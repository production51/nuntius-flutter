import 'package:nuntius_flutter/domain/status.dart';
import 'package:uuid_type/uuid_type.dart';

class Message {
  final Uuid id;
  final String senderName;
  final String receiverName;
  final String content;
  final Status status;

  const Message({
    required this.id,
    required this.senderName,
    required this.receiverName,
    required this.content,
    required this.status,
  });

  factory Message.fromJson(Map<String, dynamic> json) {
    return Message(
      id: Uuid.parse(json['id']),
      senderName: json['sender'],
      receiverName: json['receiver'],
      content: json['content'],
      status: Status.fromJson(json),
    );
  }
}
