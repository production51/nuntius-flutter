import 'package:flutter/material.dart';
import 'package:nuntius_flutter/components/user_selector.dart';
import 'package:nuntius_flutter/domain/message_manager.dart';
import 'package:nuntius_flutter/pages/screen.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
        create: (_) => MessageManager(),
        child: MaterialApp(
          title: 'Nuntius',
          theme: ThemeData(
            primarySwatch: Colors.green,
            textTheme: Theme.of(context).textTheme.copyWith(),
            errorColor: Colors.redAccent,
          ),
          home: const Screen(body: UserSelector()),
        ));
  }
}
