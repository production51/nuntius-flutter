import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:nuntius_flutter/components/message.dart';
import 'package:nuntius_flutter/components/message_sender.dart';
import 'package:nuntius_flutter/domain/message_manager.dart';
import 'package:provider/provider.dart';

class MessageList extends StatelessWidget {
  const MessageList({super.key, required this.converser});

  final String converser;

  @override
  Widget build(BuildContext context) {
    MessageManager messageManager = Provider.of<MessageManager>(context);
    return Align(
      alignment: Alignment.center,
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 2),
        constraints: const BoxConstraints(maxWidth: 500),
        child: Column(
          children: [
            Expanded(
              child: ListView(
                // TODO: scroll to end
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          converser,
                          style: TextStyle(
                            fontSize: Theme.of(context).textTheme.headlineSmall?.fontSize,
                            color: Colors.blue,
                          ),
                        ),
                        Text(
                          messageManager.user!,
                          style: TextStyle(
                            fontSize: Theme.of(context).textTheme.headlineSmall?.fontSize,
                            color: Colors.green,
                          ),
                        ),
                      ],
                    ),
                  ),
                  ...messageManager.getConversation(converser).map(
                        (dayMessages) => Column(
                          children: [
                            Container(
                              decoration: const BoxDecoration(
                                color: Colors.purple,
                                borderRadius: BorderRadius.all(Radius.circular(4)),
                              ),
                              padding: const EdgeInsets.symmetric(vertical: 2, horizontal: 4),
                              child: Text(
                                DateFormat.yMMMd().format(dayMessages.date),
                                style: const TextStyle(color: Colors.white),
                              ),
                            ),
                            ...dayMessages.messages.map(
                              (msg) => Row(
                                children: [
                                  if (messageManager.user == msg.senderName) const Spacer(flex: 2),
                                  Flexible(
                                    flex: 3,
                                    child: MessageWidget(user: messageManager.user!, message: msg),
                                  ),
                                  if (messageManager.user == msg.receiverName) const Spacer(flex: 2),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                ].toList(),
              ),
            ),
            MessageSender(
              sender: messageManager.user!,
              receiver: converser,
            ),
          ],
        ),
      ),
    );
  }
}
