import 'package:flutter/material.dart';
import 'package:nuntius_flutter/components/status.dart';
import 'package:nuntius_flutter/domain/message.dart';
import 'package:nuntius_flutter/domain/message_manager.dart';
import 'package:nuntius_flutter/domain/status.dart';
import 'package:provider/provider.dart';

class MessageWidget extends StatelessWidget {
  const MessageWidget({super.key, required this.user, required this.message});

  final String user;
  final Message message;

  @override
  Widget build(BuildContext context) {
    if (user == message.receiverName && message.status.readAt == null) {
      Provider.of<MessageManager>(context).updateMessageStatus(message.id, StatusUpdate.read);
    }
    Color backgroundColor = message.senderName == user ? Colors.green : Colors.blue;
    Color textColor = backgroundColor.computeLuminance() > 0.5 ? Colors.black : Colors.white;
    return Align(
      alignment: user == message.senderName ? Alignment.centerRight : Alignment.centerLeft,
      child: Container(
        decoration: BoxDecoration(
          color: backgroundColor,
          borderRadius: const BorderRadius.all(Radius.circular(4)),
        ),
        padding: user == message.senderName //
            ? const EdgeInsets.only(top: 4, left: 4, right: 4, bottom: 2)
            : const EdgeInsets.all(4),
        margin: const EdgeInsets.all(2),
        child: IntrinsicWidth(
          child: Column(
            children: [
              Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  message.content,
                  style: TextStyle(
                    color: textColor,
                    fontSize: Theme.of(context).textTheme.bodyLarge!.fontSize,
                  ),
                  textAlign: TextAlign.justify,
                ),
              ),
              if (user == message.senderName)
                Align(
                  alignment: Alignment.centerRight,
                  child: StatusWidget(
                    status: message.status,
                    textColor: textColor,
                  ),
                ),
            ],
          ),
        ),
      ),
    );
  }
}
