import 'package:flutter/material.dart';
import 'package:nuntius_flutter/components/message_list.dart';
import 'package:nuntius_flutter/pages/screen.dart';

class ConverserSelector extends StatefulWidget {
  const ConverserSelector({super.key});

  @override
  ConverserSelectorState createState() {
    return ConverserSelectorState();
  }
}

class ConverserSelectorState extends State<ConverserSelector> {
  final _formKey = GlobalKey<FormState>();
  final controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          Text(
            'Start a conversation with a new user:',
            style: Theme.of(context).textTheme.headlineSmall,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 48, vertical: 24),
            child: TextFormField(
              controller: controller,
              // The validator receives the text that the user has entered.
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Please enter some text';
                }
                return null;
              },
            ),
          ),
          ElevatedButton(
            onPressed: () {
              if (_formKey.currentState!.validate()) {
                ScaffoldMessenger.of(context).showSnackBar(
                  SnackBar(
                    content: Text(
                      "Created a conversation with user: ${controller.text}",
                    ),
                  ),
                );
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => Screen(
                      body: MessageList(
                        converser: controller.text,
                      ),
                    ),
                  ),
                );
              }
            },
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 8),
              child: Text(
                'Start a conversation',
                style: TextStyle(fontSize: Theme.of(context).textTheme.headlineSmall?.fontSize),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
