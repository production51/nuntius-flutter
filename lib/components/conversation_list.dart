import 'package:flutter/material.dart';
import 'package:nuntius_flutter/components/converser_selector.dart';
import 'package:nuntius_flutter/components/message_list.dart';
import 'package:nuntius_flutter/domain/message_manager.dart';
import 'package:nuntius_flutter/pages/screen.dart';
import 'package:provider/provider.dart';

class ConversationList extends StatefulWidget {
  const ConversationList({super.key});

  @override
  ConversationListState createState() {
    return ConversationListState();
  }
}

class ConversationListState extends State<ConversationList> {
  @override
  Widget build(BuildContext context) {
    // TODO: display error from response if any
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        ...Provider.of<MessageManager>(context).getConversers().map(
              (converser) => ElevatedButton(
                onPressed: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => Screen(
                        body: MessageList(
                          converser: converser.name,
                        ),
                      ),
                    ),
                  );
                },
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 8),
                  child: Text(
                    converser.unreadMessagesCount == 0 //
                        ? converser.name
                        : "${converser.name} (${converser.unreadMessagesCount})",
                    style: TextStyle(fontSize: Theme.of(context).textTheme.headlineSmall?.fontSize),
                  ),
                ),
              ),
            ),
        const ConverserSelector()
      ].toList(),
    );
  }
}
