import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:nuntius_flutter/domain/status.dart';

class StatusWidget extends StatelessWidget {
  const StatusWidget({super.key, required this.status, required this.textColor});

  final Status status;
  final Color textColor;

  @override
  Widget build(BuildContext context) {
    Icon icon = Icon(
      status.readAt != null //
          ? Icons.done_outline
          : status.deliveredAt != null //
              ? Icons.done_all
              : Icons.done,
      color: Colors.white,
      size: Theme.of(context).textTheme.bodySmall!.fontSize! * 1.2,
    );
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Padding(
          padding: const EdgeInsets.only(right: 2),
          child: Text(
            DateFormat.Hm().format(status.sentAt),
            style: TextStyle(
              color: textColor,
              fontSize: Theme.of(context).textTheme.bodySmall!.fontSize,
            ),
          ),
        ),
        icon,
      ],
    );
  }
}
