import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:nuntius_flutter/config.dart';
import 'package:nuntius_flutter/domain/response.dart';

class UserCreator extends StatefulWidget {
  const UserCreator({super.key});

  @override
  UserCreatorState createState() {
    return UserCreatorState();
  }
}

class UserCreatorState extends State<UserCreator> {
  final _formKey = GlobalKey<FormState>();
  final controller = TextEditingController();
  BackendResponse? response;

  Future<BackendResponse> postRequest(String name) async {
    return http.post(
      Uri.parse("http://$backendHost/users"),
      body: <String, String>{"name": name},
    ).then((response) {
      return BackendResponse.fromJson(json.decode(response.body));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          Text(
            'Enter a new user name:',
            style: Theme.of(context).textTheme.headlineLarge,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 48, vertical: 24),
            child: TextFormField(
              controller: controller,
              // The validator receives the text that the user has entered.
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Please enter some text';
                }
                return null;
              },
            ),
          ),
          ElevatedButton(
            onPressed: () {
              // Validate returns true if the form is valid, or false otherwise.
              if (_formKey.currentState!.validate()) {
                // If the form is valid, display a snackbar.
                ScaffoldMessenger.of(context).showSnackBar(
                  const SnackBar(content: Text('Requesting new user creation...')),
                );
                postRequest(controller.text).then((response) {
                  setState(() {
                    this.response = response;
                  });
                  ScaffoldMessenger.of(context).hideCurrentSnackBar();
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                      content: Text(
                        response.status == 201 ? 'User created successfully' : 'Error: ${response.status} ${response.error}',
                      ),
                    ),
                  );
                });
              }
            },
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 8),
              child: Text(
                'Create user',
                style: TextStyle(fontSize: Theme.of(context).textTheme.headlineSmall?.fontSize),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
