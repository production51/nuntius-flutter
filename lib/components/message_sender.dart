import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:nuntius_flutter/config.dart';
import 'package:nuntius_flutter/domain/response.dart';

class MessageSender extends StatefulWidget {
  const MessageSender({super.key, required this.sender, required this.receiver});

  final String sender;
  final String receiver;

  @override
  MessageSenderState createState() {
    return MessageSenderState();
  }
}

class MessageSenderState extends State<MessageSender> {
  final _formKey = GlobalKey<FormState>();
  final controller = TextEditingController();

  void sendMessage(String messageContent) {
    // Validate returns true if the form is valid, or false otherwise.
    if (_formKey.currentState!.validate()) {
      // If the form is valid, display a snackbar.
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text('Sending message...')),
      );
      postRequest(messageContent).then((response) {
        ScaffoldMessenger.of(context).hideCurrentSnackBar();
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(
              response.status == 201 //
                  ? 'Message sent successfully'
                  : 'Error: ${response.status} ${response.error}',
            ),
          ),
        );
      });
      controller.clear();
    }
  }

  Future<BackendResponse> postRequest(String content) async {
    return http.post(
      Uri.parse("http://$backendHost/messages"),
      body: <String, String>{
        "sender": widget.sender,
        "receiver": widget.receiver,
        "content": content,
      },
    ).then((response) {
      return BackendResponse.fromJson(json.decode(response.body));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Padding(
        padding: const EdgeInsets.only(left: 48, right: 32, bottom: 8),
        child: Row(
          children: <Widget>[
            Expanded(
              child: TextFormField(
                controller: controller,
                // The validator receives the text that the user has entered.
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter some text';
                  }
                  return null;
                },
                maxLength: 512,
                onFieldSubmitted: sendMessage,
                minLines: 1,
                maxLines: 10,
                textInputAction: TextInputAction.done,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 16),
              child: IconButton(
                icon: const Icon(
                  Icons.send,
                  color: Colors.green,
                ),
                onPressed: () => sendMessage(controller.text),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
