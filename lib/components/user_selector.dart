import 'package:flutter/material.dart';
import 'package:flutter_session_manager/flutter_session_manager.dart';
import 'package:nuntius_flutter/components/conversation_list.dart';
import 'package:nuntius_flutter/domain/message_manager.dart';
import 'package:nuntius_flutter/pages/screen.dart';
import 'package:provider/provider.dart';

class UserSelector extends StatefulWidget {
  const UserSelector({Key? key});

  @override
  UserSelectorState createState() {
    return UserSelectorState();
  }
}

class UserSelectorState extends State<UserSelector> {
  final _formKey = GlobalKey<FormState>();
  final controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          Text(
            'Enter your user name:',
            style: Theme.of(context).textTheme.headlineLarge,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 48, vertical: 24),
            child: TextFormField(
              controller: controller,
              // The validator receives the text that the user has entered.
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Please enter some text';
                }
                return null;
              },
            ),
          ),
          ElevatedButton(
            onPressed: () {
              if (_formKey.currentState!.validate()) {
                // LOCALLY set user name across frontend app, no backend interaction *yet*
                SessionManager().set("user", controller.text).then((response) {
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                      content: Text(
                        "Selected user: ${controller.text}",
                      ),
                    ),
                  );
                  Provider.of<MessageManager>(context, listen: false).setUser(controller.text);
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => const Screen(body: ConversationList()),
                    ),
                  );
                });
              }
            },
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 8),
              child: Text(
                'Select User',
                style: TextStyle(fontSize: Theme.of(context).textTheme.headlineSmall?.fontSize),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
