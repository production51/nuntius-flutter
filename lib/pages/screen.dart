import 'package:flutter/material.dart';
import 'package:nuntius_flutter/components/user_creator.dart';
import 'package:nuntius_flutter/components/user_selector.dart';

class Screen extends StatelessWidget {
  const Screen({super.key, this.body});

  final Widget? body;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Nuntius",
          style: TextStyle(
            fontSize: Theme.of(context).textTheme.headlineSmall?.fontSize,
            fontWeight: FontWeight.bold,
            color: Colors.white,
          ),
        ),
        backgroundColor: Colors.orange,
        leading: Navigator.of(context).canPop() //
            ? IconButton(
                icon: const Icon(Icons.arrow_back, color: Colors.white),
                onPressed: () => Navigator.of(context).pop(),
              )
            : null,
      ),
      drawer: Drawer(
        // Add a ListView to the drawer. This ensures the user can scroll
        // through the options in the drawer if there isn't enough vertical
        // space to fit everything.
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: [
            DrawerHeader(
              decoration: const BoxDecoration(
                color: Colors.green,
              ),
              child: Text(
                'Navigation',
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: Theme.of(context).textTheme.headlineSmall?.fontSize,
                ),
              ),
            ),
            const DrawerItem(iconData: Icons.person_add, title: "New User", child: UserCreator()),
            const DrawerItem(iconData: Icons.message, title: "Messages", child: UserSelector()),
          ],
        ),
      ),
      body: body,
    );
  }
}

class DrawerItem extends StatelessWidget {
  const DrawerItem({super.key, required this.title, required this.iconData, required this.child});

  final String title;
  final IconData iconData;
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Row(
        children: [
          Padding(
            padding: const EdgeInsets.only(right: 8),
            child: Icon(iconData),
          ),
          Text(title),
        ],
      ),
      onTap: () {
        Navigator.of(context).pushReplacement(
          MaterialPageRoute(
            builder: (context) => Screen(body: child),
          ),
        );
      },
    );
  }
}
